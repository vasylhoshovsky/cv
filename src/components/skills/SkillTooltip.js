import React from 'react';
import ClassNames from "classnames";
import ReactTooltip from 'react-tooltip';
import { getFormatedStringFromDays } from "../utils/ExperienceCalculationUtils";

export const generateId = (name) => {
    return 'tooltip-' + name.replace(/ /g, "_");
};

const SkillTooltip = (props) => {

    const {name, experiencePercent, techExperience, totalExperience} = props.skill;

    const circleClassName = ClassNames(
        "c100 small dark",
        {"green": experiencePercent >= 70},
        {"orange": experiencePercent >= 40 && experiencePercent <= 60},
        {"red": experiencePercent < 40},
        "p" + experiencePercent
    );

    return (
        <ReactTooltip id={generateId(name)}
                      className="skill-tooltip"
                      clickable>
            <div className="title">
                {name}
            </div>

            <div className={circleClassName}>
                <span className="rate">{experiencePercent + '%'}</span>
                <div className="slice">
                    <div className="bar"/>
                    <div className="fill"/>
                </div>
            </div>

            <div className="description">
                {
                    experiencePercent > 0 ?
                    `${getFormatedStringFromDays(techExperience)} of commercial usage which is ${experiencePercent} % of my total experience (${getFormatedStringFromDays(totalExperience)})` :
                        `No commercial experience - used just for fun`

                }
            </div>
        </ReactTooltip>
    );
};

export default SkillTooltip;