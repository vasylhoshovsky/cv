import React, { Fragment } from 'react';
import SkillTooltip, { generateId } from "./SkillTooltip";

const SkillForm = (props) => {

    const { name, icon } = props.skill;

    return (
        <Fragment>
            <figure className="skill"
                    data-tip=""
                    data-for={ generateId(name) }>

                <img src={ icon } alt={ name }/>
                <figcaption>{ name }</figcaption>
            </figure>

            <SkillTooltip skill={ props.skill }/>
        </Fragment>
    );
};

export default SkillForm;