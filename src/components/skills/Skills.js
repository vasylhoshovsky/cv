import React from 'react';
import SectionHeader from "../common/SectionHeader";
import { config } from "../../constants/config";
import SkillGroupForm from "./SkillGroupForm";
import { getExperienceOnWorkplaces, getTotalExperience } from "../utils/ExperienceCalculationUtils";

const Skills = () => {

    const getSkills = () => {
        const totalExperience = getTotalExperience();

        return config.skills.map((group) => {
            const items = populateWithExperience(group.items, totalExperience);
            return {...group, items};
        });
    };

    const populateWithExperience = (skills, totalExperience) => {
        return skills.map(s => {
            const workplaces = config.experience.filter(e => experienceWithTechnologyPredicate(e, s.name));
            const techExperience = getExperienceOnWorkplaces(workplaces);
            const experiencePercent = ((techExperience / totalExperience) * 100).toFixed(0);

            return {...s, experiencePercent, techExperience, totalExperience};
        });
    };

    const experienceWithTechnologyPredicate = (workplace, skillName) => {
        const technologyNames = workplace.technologies.map(t => t.name);
        return technologyNames.includes(skillName);
    };


    const skills = getSkills();

    return (
        <section className="skills">
            <SectionHeader title="Skills"/>

            {
                skills.map((g, i) => <SkillGroupForm key={i} skillGroup={g}/>)
            }
        </section>
    );
};

export default Skills;