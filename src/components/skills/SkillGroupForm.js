import React from 'react';
import SkillForm from "./SkillForm";

const SkillGroupForm = (props) => {

    const { category, items } = props.skillGroup;

    const indents = Array.apply(null, {length: 25}).map(Number.call, Number);

    return (
        <div className="skill-group">
            <div className="category-title">{ category }</div>

            {
                items.map((s, i) => <SkillForm key={i} skill={s}/>)
            }

            {
                indents.map((s, i) => <div className='indent' key={`indent${i}`} />)
            }

        </div>
    );
};

export default SkillGroupForm;