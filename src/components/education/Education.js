import React from 'react';
import SectionHeader from "../common/SectionHeader";
import { config } from "../../constants/config";
import EducationForm from "./EducationForm";

const Education = () => {

    return (
        <section className="education">
            <SectionHeader title="Education"/>
            
            <ul className="timeline">
                {
                    config.education.map((e, i) => <EducationForm key={ "edu" + i } educationItem={ e }/>)
                }
            </ul>
        </section>
    );
};

export default Education;