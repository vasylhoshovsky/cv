import React from 'react';
import ActivityHeader from "../common/ActivityHeader";

const EducationForm = (props) => {

    const { course, degree, companyName, startDate, endDate } = props.educationItem;

    return (
        <li className="education-item item">

            <ActivityHeader course={ course }
                            degree={ degree }
                            companyName={ companyName }
                            startDate={ startDate }
                            endDate={ endDate }/>

        </li>
    );
};

export default EducationForm;