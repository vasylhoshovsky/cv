import React from 'react';

const NameForm = (props) => {

    return (
        <div className="name-container">
            <div className="name">{ props.name }</div>
            <div className="position">{ props.position }</div>
        </div>
    );
};

export default NameForm;