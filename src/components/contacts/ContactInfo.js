import React from 'react';
import moment from 'moment/moment';
import { config } from '../../constants/config';
import ContactItemForm from "./ContactItemForm";
import NameForm from "./NameForm";
import { getFormatedStringFromDays, getTotalExperience } from "../utils/ExperienceCalculationUtils";

const ContactInfo = () => {

    const { name, position, birthday, location, phone, email, skype, photo } = config;

    const totalExperience = getTotalExperience();

    return (
        <section className="contact-info">
            <figure className="photo-wrapper">
                <img className="photo" src={ photo } alt=""/>
            </figure>

            <aside className="contact-items">
                <NameForm name={ name }
                          position={ position }/>


                <ContactItemForm fieldName="Birthday"
                                 fieldValue={ moment(birthday).format("MMMM DD, YYYY") }/>

                <ContactItemForm fieldName="Age"
                                 fieldValue={ moment().diff(birthday, 'years') }/>

                <ContactItemForm fieldName="Total experience"
                                 fieldValue={ getFormatedStringFromDays(totalExperience) }/>

                <ContactItemForm fieldName="Location"
                                 fieldValue={ location }/>

                <ContactItemForm fieldName="Phone"
                                 fieldValue={ <a href={ `tel:${ phone }` }>{ phone }</a> }/>

                <ContactItemForm fieldName="Skype"
                                 fieldValue={ <a href={ `skype:${ skype }` }>{ skype }</a> }/>

                <ContactItemForm fieldName="E-mail"
                                 fieldValue={ <a href={ `mailto:${ email }` }>{ email }</a> }/>
            </aside>

        </section>
    );
};

export default ContactInfo;