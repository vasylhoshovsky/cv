import React from 'react';

const ContactItemForm = (props) => {

    return (
        <div className="contact-item">
            <span className="field-name">{ props.fieldName }: </span>
            <span className="field-value">{ props.fieldValue }</span>
        </div>
    );
};

export default ContactItemForm;