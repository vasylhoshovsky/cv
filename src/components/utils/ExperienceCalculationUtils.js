import { config } from "../../constants/config";
import moment from "moment";


export const getTotalExperience = () => {
    return getExperienceOnWorkplaces(config.experience);
};

export const getExperienceOnWorkplaces = (workplcaces) => {
    return workplcaces.reduce((acc, workplace) => {
        const workedDays = moment.duration(workplace.endDate.diff(workplace.startDate)).asDays();
        return acc + workedDays;
    }, 0);
};

export const getFormatedStringFromDays = (numberOfDays) => {
    const years = Math.floor(numberOfDays / 365);
    const months = Math.floor(numberOfDays % 365 / 30);

    const yearsDisplay = years > 0 ? years + (years === 1 ? " year" : " years") : "";
    const monthsDisplay = months > 0 ? months + (months === 1 ? " month" : " months") : "";
    const separator = yearsDisplay && monthsDisplay ? " and " : "";
    return `${yearsDisplay}${separator}${monthsDisplay}`;
};
