import React from 'react';
import Experience from "./experience/Experience";
import Education from "./education/Education";
import ContactInfo from "./contacts/ContactInfo";
import Intro from "./intro/Intro";
import Skills from "./skills/Skills";
import Languages from "./languages/Languages";

const Main = () => {

    return (
        <main className="container">
            <ContactInfo/>
            <Intro/>
            <Skills/>
            <Languages/>
            <Experience/>
            <Education/>
        </main>
    );
};

export default Main;