import React from 'react';
import loader from '../../assets/loader.gif'

const Loader = (props) => {

    return (
        <figure className="loader">
            <img className="spinner" src={loader}  alt=""/>
        </figure>
    );
};

export default Loader;