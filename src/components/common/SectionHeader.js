import React from 'react';

const SectionHeader = (props) => {

    return (
        <div className="section-header">
            {props.title}
        </div>
    );
};

export default SectionHeader;