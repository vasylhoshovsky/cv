import React from 'react';
import { DATE_FORMAT_SHORT } from "../../constants/constants";
import moment from "moment";
import ClassNames from "classnames";

const ActivityHeader = (props) => {

    const { position, course, degree, companyName, startDate, endDate, expandable, toggleDetails } = props;

    const start = startDate.format(DATE_FORMAT_SHORT);
    const end = moment().isSame(endDate, 'd') ? "Present" : endDate.format(DATE_FORMAT_SHORT);

    const onClickHeader = () => {
        expandable && toggleDetails && toggleDetails();
    };

    return (
        <div className={ClassNames("activity-header", { expandable })} onClick={ onClickHeader }>

            <div className="title">
                { position || course }
            </div>

            {
                degree && <div className="degree">{ degree }</div>
            }

            <div className="company">
                { companyName }
            </div>

            <span className="dates">
                { `${ start } - ${ end }` }
            </span>

        </div>
    );
};

export default ActivityHeader;