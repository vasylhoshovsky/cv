import React from 'react';

const LanguageForm = (props) => {

    const {name, level, icon} = props.language;

    return (
        <figure className="language">
            <img src={icon} alt="" />
            <figcaption>{`${name} (${level})`}</figcaption>
        </figure>
    );
};

export default LanguageForm;