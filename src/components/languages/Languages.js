import React from 'react';
import SectionHeader from "../common/SectionHeader";
import { config } from "../../constants/config";
import LanguageForm from "./LanguageForm";

const Languages = () => {

    return (
        <section className="languages">
            <SectionHeader title="Languages"/>

            {
                config.languages.map((l, i)=> <LanguageForm key={i} language={l} />)
            }
        </section>
    );
};

export default Languages;