import React from 'react';
import { config } from '../../constants/config';
import SectionHeader from "../common/SectionHeader";

const Intro = () => {

    return (
        <section className="intro">
            <SectionHeader title="Intro"/>

            {
                config.introText.map((t, i)=> <p key={i}>{t}</p>)
            }

        </section>
    );
};

export default Intro;