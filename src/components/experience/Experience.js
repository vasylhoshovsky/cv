import React from 'react';
import SectionHeader from "../common/SectionHeader";
import { config } from "../../constants/config";
import WorkPlaceForm from "./WorkPlaceForm";

const Experience = () => {

    return (
        <section className="experience">
            <SectionHeader title="Experience"/>

            <ul className="timeline">
                {
                    config.experience.map((e, i) => <WorkPlaceForm key={ "exp" + i } experienceItem={ e }/>)
                }
            </ul>
        </section>
    );
};

export default Experience;