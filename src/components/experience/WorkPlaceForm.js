import React, { useState } from 'react';
import ClassNames from "classnames";
import ActivityHeader from "../common/ActivityHeader";
import { generateId } from "../skills/SkillTooltip";

const WorkPlaceForm = (props) => {

    const { position, companyName, startDate, endDate, technologies, responsibilities } = props.experienceItem;

    const [expanded, setExpanded] = useState(true);

    const toggleDetails = () => {
        setExpanded(!expanded);
    };

    return (
        <li className="experience-item item">

            <ActivityHeader position={ position }
                            companyName={ companyName }
                            startDate={ startDate }
                            endDate={ endDate }
                            expandable
                            toggleDetails={ toggleDetails }/>

            <div className={ ClassNames("details", { expanded }) }>
                <div className="technologies">
                    {
                        technologies.map((t, i) => {
                            return (
                                <figure className="technology"
                                        key={ i }
                                        data-tip=""
                                        data-for={ generateId(t.name) }>
                                    <img src={ t.icon } alt={ t.name }/>
                                </figure>
                            );
                        })
                    }
                </div>


                <ul className="responsibilities">
                    {
                        responsibilities.map((r, i) => {
                            return (
                                <li className="responsibility" key={ i }>
                                    { r }
                                </li>
                            );
                        })
                    }
                </ul>
            </div>

        </li>
    );
};

export default WorkPlaceForm;