import moment from 'moment';
import mainPhoto from '../assets/main-photo.jpg';
import { languages, technologies } from "./constants";

export const TIME_FLOW_START = moment('2010-09-01');

export const config = {
    name: 'Vasyl Hoshovskyi',
    position: 'Software developer',
    birthday: moment('1992-12-29'),
    location: 'Kyiv',
    phone: '+380635831136',
    email: 'vasyl.hoshovsky@gmail.com',
    skype: 'vasylhoshovsky',
    photo: mainPhoto,

    introText: [
        "Hello there!",
        "You're probably looking for software developer. Here I am! Please contact me if you need Java and/or JavaScript developer. I have some experience with those technologies.",
        "I had a chance to work as java and full-stack developer along with talented  teammates on different projects. I've gained the most valuable experience working on banking and e-commerce projects.",
        "My area of responsibility includes development of new features, business logic implementing, code reviews, test coverage, etc.",
        "I like coding and enjoy seeing result of my work. Never stop learning and always open for something new (except php).",
        "I'm looking for challenging position where I can grow professionally and apply my skills."
    ],

    skills: [
        {
            category: "Programing Languages / Technologies",
            items: [
                technologies.java,
                technologies.js,
                technologies.html,
                technologies.css,
                technologies.less,
                technologies.scss,
                technologies.restApi
            ]
        },

        {
            category: "Databases",
            items: [
                technologies.oracle,
                technologies.mySQL,
                technologies.mongoDB,
                technologies.cassandra
            ]
        },

        {
            category: "Frameworks / Libraries",
            items: [
                technologies.springFramework,
                technologies.hibernate,
                technologies.react,
                technologies.jQuery,
                technologies.angular,
                technologies.angularJs,
                technologies.jUnit,
                technologies.mockito,
                technologies.flyway,
                technologies.liquibase
            ]
        },

        {
            category: "Tools",
            items: [
                technologies.tomcat,
                technologies.nodejs,
                technologies.npm,
                technologies.maven,
                technologies.webpack,
                technologies.jenkins,
                technologies.git,
                technologies.jira,
                technologies.linux
            ]
        }
    ],

    languages: [
        languages.english,
        languages.ukrainian,
        languages.russian,
        languages.polish
    ],

    experience: [
        {
            position: "Software Developer",
            companyName: "Luxoft Ukraine",
            startDate: moment('2020-06-09'),
            endDate: moment(),
            responsibilities: [
                "Working with several bank projects that help internal users to do their routine connected to data entity creation, their management through specific workflows and report generation based on current data",
                "Working as full stack developer",
                "Develop web-based applications front-to-back including database",
                "Estimation and prioritizing stories and tasks",
                "Communication with business analysts - requirements analysis and clarification",
                "Feature development",
                "Project support and bug fix",
                "Maintaining of high test coverage",
                "Automation test coverage and maintenance",
                "Implementing of full feature including backend and frontend",
                "Code review",
                "Project documentation maintenance"
            ],
            technologies: [
                technologies.java,
                technologies.js,
                technologies.html,
                technologies.css,
                technologies.scss,
                technologies.tomcat,
                technologies.webpack,
                technologies.npm,
                technologies.oracle,
                technologies.springFramework,
                technologies.hibernate,
                technologies.restApi,
                technologies.jUnit,
                technologies.mockito,
                technologies.angular,
                technologies.angularJs,
                technologies.jQuery,
                technologies.jira,
                technologies.git,
                technologies.linux
            ]
        },

        {
            position: "Software Developer",
            companyName: "Daxx",
            startDate: moment('2018-02-12'),
            endDate: moment('2020-05-19'),
            responsibilities: [
                "Working on the project in domain of logistic and e-commerce",
                "Working as full stack developer",
                "Architecture and design",
                "Requirements analysis and clarification",
                "Estimation and prioritizing stories and tasks",
                "Feature development",
                "Bug fix and support of legacy parts of application",
                "Refactoring and migrating old functionality (e.g. Jsp to ReactJS, SOAP API to REST API)",
                "Code review",
                "Project documentation maintenance, assistance for technical writer"
            ],
            technologies: [
                technologies.java,
                technologies.js,
                technologies.html,
                technologies.css,
                technologies.scss,
                technologies.maven,
                technologies.webpack,
                technologies.jenkins,
                technologies.nodejs,
                technologies.npm,
                technologies.restApi,
                technologies.springFramework,
                technologies.hibernate,
                technologies.jUnit,
                technologies.mockito,
                technologies.react,
                technologies.jQuery,
                technologies.flyway,
                technologies.mySQL,
                technologies.mongoDB,
                technologies.cassandra,
                technologies.git,
                technologies.jira
            ]
        },

        {
            position: "Software Developer",
            companyName: "Luxoft Ukraine",
            startDate: moment('2015-12-28'),
            endDate: moment('2018-02-09'),
            responsibilities: [
                "Working on bank platform which implements complicated business logic to serve internal bank processes and workflows that helps users to achieve desired result in their daily routine",
                "Working as full stack developer",
                "Estimation and prioritizing stories and tasks",
                "Communication with business analysts - requirements analysis and clarification",
                "Feature development",
                "Project support and bug fix",
                "Maintaining of high test coverage",
                "Implementing of full feature including backend and frontend",
                "Code review",
                "Project documentation maintenance"
            ],
            technologies: [
                technologies.java,
                technologies.js,
                technologies.html,
                technologies.css,
                technologies.less,
                technologies.tomcat,
                technologies.webpack,
                technologies.npm,
                technologies.oracle,
                technologies.springFramework,
                technologies.hibernate,
                technologies.restApi,
                technologies.liquibase,
                technologies.jUnit,
                technologies.mockito,
                technologies.react,
                technologies.jQuery,
                technologies.jira,
                technologies.git
            ]
        },

        {
            position: "Software Developer",
            companyName: "Thinkmobiles",
            startDate: moment('2015-07-01'),
            endDate: moment('2015-12-25'),
            responsibilities: [
                "Working on creation of e-commerce platform",
                "Working as full stack developer",
                "Feature development",
                "Project support and bug fix",
                "Communication with customer"
            ],
            technologies: [
                technologies.java,
                technologies.js,
                technologies.html,
                technologies.css,
                technologies.tomcat,
                technologies.mySQL,
                technologies.springFramework,
                technologies.hibernate,
                technologies.angularJs,
                technologies.git
            ]
        },

        {
            position: "Java Developer",
            companyName: "Intellectual Financial Systems",
            startDate: moment('2013-08-12'),
            endDate: moment('2015-02-06'),
            responsibilities: [
                "Development backend for CRM system",
                "Covering code with tests"
            ],
            technologies: [
                technologies.java,
                technologies.tomcat,
                technologies.springFramework,
                technologies.hibernate,
                technologies.mySQL,
                technologies.jUnit,
                technologies.mockito,
                technologies.git
            ]
        }
    ],

    education: [
        {
            course: "English (B1 - B2)",
            companyName: "Speak Up",
            startDate: moment('2017-04-04'),
            endDate: moment('2017-09-20')
        },
        {
            course: "Jurisprudence",
            degree: "Law, degree of master",
            companyName: "Interregional Academy of Personnel Management",
            startDate: moment('2014-09-01'),
            endDate: moment('2015-09-15')
        },
        {
            course: "Java core",
            companyName: "Codefire",
            startDate: moment('2013-02-04'),
            endDate: moment('2013-06-07')
        },
        {
            course: "Commercial and labor law",
            degree: "Law, degree of bachelor",
            companyName: "Interregional Academy of Personnel Management",
            startDate: moment('2010-09-01'),
            endDate: moment('2014-06-30')
        }
    ]


};
