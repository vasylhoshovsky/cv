import javaIcon from '../assets/tech-icons/java.jpg';
import nodejsIcon from '../assets/tech-icons/nodejs.png';
import restApiIcon from '../assets/tech-icons/rest-api.jpg';
import springFrameworkIcon from '../assets/tech-icons/spring-framework.svg';
import hibernateIcon from '../assets/tech-icons/hibernate.png';
import jUnitIcon from '../assets/tech-icons/junit.png';
import mockitoIcon from '../assets/tech-icons/mockito.jpg';
import oracleIcon from '../assets/tech-icons/oracle.png';
import mysqlIcon from '../assets/tech-icons/mysql.png';
import mongodbIcon from '../assets/tech-icons/mongodb.png';
import cassandraIcon from '../assets/tech-icons/cassandra.png';
import jsIcon from '../assets/tech-icons/js.jpg';
import htmlIcon from '../assets/tech-icons/html.png';
import cssIcon from '../assets/tech-icons/css.png';
import lessIcon from '../assets/tech-icons/less.png';
import scssIcon from '../assets/tech-icons/scss.png';
import reactIcon from '../assets/tech-icons/react.png';
import jqueryIcon from '../assets/tech-icons/jquery.png';
import mavenIcon from '../assets/tech-icons/maven.png';
import webpackIcon from '../assets/tech-icons/webpack.png';
import jenkinsIcon from '../assets/tech-icons/jenkins.jpg';
import gitIcon from '../assets/tech-icons/git.png';
import jiraIcon from '../assets/tech-icons/jira.png';
import linuxIcon from '../assets/tech-icons/linux.png';
import angularJsIcon from '../assets/tech-icons/angular.png';
import angularIcon from '../assets/tech-icons/angular2.png';
import flywayIcon from '../assets/tech-icons/flyway.png';
import liquibaseIcon from '../assets/tech-icons/liquibase.png';
import tomcatIcon from '../assets/tech-icons/tomcat.png';
import npmIcon from '../assets/tech-icons/npm.png';

import gb from '../assets/language-icons/gb.svg';
import ua from '../assets/language-icons/ua.svg';
import ru from '../assets/language-icons/ru.svg';
import pl from '../assets/language-icons/pl.svg';

export const DATE_FORMAT_SHORT = "MMM, YYYY";

export const technologies = {
    java: {
        name: "Java",
        icon: javaIcon
    },
    nodejs: {
        name: "Node.JS",
        icon: nodejsIcon
    },
    npm: {
        name: "npm",
        icon: npmIcon
    },
    restApi: {
        name: "REST API",
        icon: restApiIcon
    },
    springFramework: {
        name: "Spring Framework",
        icon: springFrameworkIcon
    },
    hibernate: {
        name: "Hibernate",
        icon: hibernateIcon
    },
    jUnit: {
        name: "JUnit",
        icon: jUnitIcon
    },
    mockito: {
        name: "Mockito",
        icon: mockitoIcon
    },
    flyway: {
        name: "Flyway",
        icon: flywayIcon
    },
    liquibase: {
        name: "Liquibase",
        icon: liquibaseIcon
    },
    oracle: {
        name: "Oracle DB",
        icon: oracleIcon
    },
    mySQL: {
        name: "MySQL",
        icon: mysqlIcon
    },
    mongoDB: {
        name: "Mongo DB",
        icon: mongodbIcon
    },
    cassandra: {
        name: "Apache Cassandra",
        icon: cassandraIcon
    },


    js: {
        name: "JavaScript",
        icon: jsIcon
    },
    html: {
        name: "HTML",
        icon: htmlIcon
    },
    css: {
        name: "CSS",
        icon: cssIcon
    },
    less: {
        name: "LESS",
        icon: lessIcon
    },
    scss: {
        name: "SCSS",
        icon: scssIcon
    },
    react: {
        name: "React",
        icon: reactIcon
    },
    jQuery: {
        name: "jQuery",
        icon: jqueryIcon
    },
    angularJs: {
        name: "AngularJS (1.x)",
        icon: angularJsIcon
    },
    angular: {
        name: "Angular",
        icon: angularIcon
    },


    maven: {
        name: "Maven",
        icon: mavenIcon
    },
    webpack: {
        name: "Webpack",
        icon: webpackIcon
    },
    tomcat: {
        name: "Tomcat",
        icon: tomcatIcon
    },
    jenkins: {
        name: "Jenkins",
        icon: jenkinsIcon
    },
    git: {
        name: "Git",
        icon: gitIcon
    },
    jira: {
        name: "Jira",
        icon: jiraIcon
    },
    linux: {
        name: "Linux",
        icon: linuxIcon
    }
};


export const languages = {
    english: {
        name: "English",
        level: "B2 - Upper-intermediate",
        icon: gb
    },
    ukrainian: {
        name: "Ukrainian",
        level: "C2 - Proficient / Mother tongue",
        icon: ua
    },
    russian: {
        name: "Russian",
        level: "C2 - Proficient",
        icon: ru
    },
    polish: {
        name: "Polish",
        level: "A0 - Beginner",
        icon: pl
    }
};
