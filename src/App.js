import React, { useState, useEffect } from 'react';
import Main from "./components/Main";
import Loader from "./components/common/Loader";

import "./styles/index.less";

const App = () => {

    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setTimeout(setLoading.bind(null, false), 500);
    }, []);

    return (
        loading ? <Loader/> : <Main/>
    );
};

export default App;