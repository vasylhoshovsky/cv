#!/usr/bin/env node

const path = require('path');
const http = require('http');
const express = require('express');

const app = express();

const httpPort = process.env.PORT || 80;


app.use('/static', express.static(path.join(__dirname, './dist')));

app.get("*", (req, res) => {
    res.sendFile(__dirname + '/index.html');
});


const httpServer = http.createServer(app);

httpServer.listen(httpPort);